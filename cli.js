/*
* Copyright (c) 2018 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

//
// Script.js for help create a new web components based on Stencil
//

const inquirer = require('inquirer');
const chalk = require("chalk");
const figlet = require("figlet");
const replaceInFiles = require('replace-in-files');
const path = require('path');
const fs = require('fs-extra');
const changeCase = require('change-case');
const renamify = require('renamify');

// Dir from new components and base components
const baseDir = path.resolve(__dirname, './src/src_components/components/base-component');
const componentsDir = path.resolve(__dirname, './src/components');

const Init = () => {
  console.log(
    chalk.green(
      figlet.textSync("NUIverse CLI", {
        font: "Ghost",
        horizontalLayout: "default",
        verticalLayout: "default"
      })
    )
  );
}

const Asks = () => {
  const questions = [
    {
      name: "componentName",
      type: "input",
      message: "What is the name of the new Web Component?"
    },
  ];
  return inquirer.prompt(questions);
}

const createComponent = (componentName) => {
  let fixedName = changeCase.paramCase(componentName);
  let newDir = path.resolve(componentsDir, fixedName);

  fs.copy(baseDir, newDir)
    .then(() => {
      // Rename components and content
      updateFiles(componentName, newDir, fixedName);
    })
    .catch(err => console.error(err))
    
    return newDir;
}

const updateFiles = async (componentName, newDir, fixedName) => {
  let pascalCase = changeCase.pascalCase(componentName);

  try {
    const {
      changedFiles,
      countOfMatchesByPaths,
      replaceInFilesOptions 
    } = await replaceInFiles({
      files: newDir,
      from: /base-component/g,
      to: fixedName,
    },)
      .pipe({
        files: newDir,
        from: /BaseComponent/g,
        to: pascalCase,
      })

      const from = [
          'base-component.css',
          'base-component.e2e.ts',
          'base-component.tsx'
      ];
      
      const to = [
        fixedName + '.css',
        fixedName + '.e2e.ts',
        fixedName + '.tsx'
      ];
      
      renamify(newDir, from, to)
          .catch(console.error())

    return;

  } catch (error) {
    console.log('Error occurred:', error);
  }
};

const success = componentPath => {
  console.log(
    chalk.white.bgGreen.bold(`Done! Your new web compoent was created at ${componentPath} :)`)
  );
}

const run = async () => {
  
  // Show welcome message
  Init();

  // Ask question
  const answer = await Asks();
  const { componentName } = answer;

  // Create new component
  const componentPath = createComponent(componentName);

  // Show success message
  success(componentPath);
};

// Run CLI
run();
