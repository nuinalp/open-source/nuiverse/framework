'use strict'
const path = require('path')
const config = require('./config')
const packageConfig = require('../package.json')

exports.resolve = function (_path) {
  return path.join(__dirname, '..', _path)
}

exports.assetsPath = function (_path) {
  const assetsSubDirectoryCSS = process.env.NODE_ENV === 'production'
    ? config.build.assetsSubDirectoryCSS
    : config.dev.assetsSubDirectory

  const assetsSubDirectoryJS = process.env.NODE_ENV === 'production'
  ? config.build.assetsSubDirectoryJS
  : config.dev.assetsSubDirectory
  
  let finalPath

    if (_path.match(/.css/)) {
      finalPath = path.posix.join(assetsSubDirectoryCSS, _path)
    } else {
      finalPath = path.posix.join(assetsSubDirectoryJS, _path)
    }

  return finalPath;
}

exports.createNotifierCallback = () => {
  const notifier = require('node-notifier')

  return (severity, errors) => {
     if (severity !== 'error') return

    const error = errors[0]
    const filename = errorle && error.file.split('!').pop()

    notifier.notify({
      title: packageConfig.name,
      message: severity + ': ' + error.name,
      subtitle: filename || '',
      icon: path.join(__dirname, 'logo.png')
    })
  }
}
